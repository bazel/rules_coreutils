# `rules_coreutils`

> A Bazel ruleset for providing `coreutils` such as `cp`, `mkdir`, etc

## Getting Started

Add the following to `MODULE.bazel`:

```py
bazel_dep(name="rules_coreutils", version="0.0.0")
```

### Toolchains

Each core utility is split into a toolchain, specify the toolchain on the rule:

```py
def implementation(ctx):
    cp = ctx.toolchains["@rules_coreutils//coreutils/toolchain/cp:type"]
    ctx.actions.run(
        tools = [cp.run],
    )

something = rule(
    implementation = implementation,
    toolchains = [
        "@rules_coreutils//coreutils/toolchain/cp:type",
    ],
)
```

### Entrypoint

The `coreutils` multi-call binary is exported for use in hermetic repository rules.

Use the repository from the export extension in `MODULE.bazel`:

```py
export = use_extension("@toolchain_utils//toolchain/export:defs.bzl", "toolchain_export")
use_repo(export, "coreutils")

some_repo_rule(
    coreutils = "@coreutils//:entrypoint",
)
```

### Runnable Targets

The resolved toolchains are runnable:

```sh
bazel run -- @rules_coreutils//coreutils/toolchain:resolved
```

## Hermeticity

The module is fully hermetic. No Bash is required on Windows.

## Implementations

Various implementations of the core utilties are resolved depending on the platform.

Some implementation only implement the POSIX specified arguments and behaviours.

Toolchains _may_ resolve to particular implementations such as GNU on certain platforms.

It is recommended to stick to the [POSIX specified usage][posix] of the core utilities.

[posix]: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap04.html#tag_20
