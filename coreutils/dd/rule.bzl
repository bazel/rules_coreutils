visibility("//coreutils/...")

ATTRS = {
    "src": attr.label(
        doc = "The file to copy into the output file.",
        allow_single_file = True,
        mandatory = True,
    ),
}

def implementation(ctx):
    toolchain = ctx.toolchains["//coreutils/toolchain/dd:type"]

    output = ctx.actions.declare_file(ctx.label.name)

    args = ctx.actions.args()
    args.add(ctx.file.src, format = "if=%s")
    args.add(output, format = "of=%s")

    ctx.actions.run(
        mnemonic = "Dd",
        inputs = [ctx.file.src],
        tools = [toolchain.executable],
        executable = toolchain.executable,
        arguments = [args],
        outputs = [output],
        toolchain = "//coreutils/toolchain/dd:type",
    )

    default = DefaultInfo(
        files = depset([output]),
        runfiles = ctx.runfiles([output]),
    )

    return [default]

dd = rule(
    doc = "Copies file(s).",
    attrs = ATTRS,
    implementation = implementation,
    toolchains = ["//coreutils/toolchain/dd:type"],
)
