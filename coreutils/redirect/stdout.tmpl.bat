@echo off

:: Enable Batch extensions
verify other 2>nul
setlocal EnableExtensions
if errorlevel 1 (
  echo "Failed to enable extensions"
  exit /b 120
)

:: Enable delayed expansion of variables with `!VAR!`
verify other 2>nul
setlocal EnableDelayedExpansion
if errorlevel 1 (
  echo "Failed to enable extensions"
  exit /b 120
)

:: Bazel substitutions
for /f %%a in ("{{stdout}}") do set "STDOUT=%%~fa"

:: Convert executable to backslash path
set EXE=%1
set EXE=%EXE:/=\%
set ARGS=%*
set ARGS=!ARGS:%1=%EXE%!

:: Redirect the command
%ARGS% >"%STDOUT%"
if %%ERRORLEVEL%% neq 1 exit /b %%ERRORLEVEL%%
