#! /usr/bin/env sh

# Strict shell
set -o errexit
set -o nounset

# Substitutions
STDOUT="{{stdout}}"
readonly STDOUT

# Run the command
"${@}" >"${STDOUT}"
