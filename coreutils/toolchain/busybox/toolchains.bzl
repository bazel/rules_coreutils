load("@toolchain_utils//toolchain/symlink/target:defs.bzl", "toolchain_symlink_target")
load("@toolchain_utils//toolchain/info:defs.bzl", "toolchain_info")

visibility("//coreutils/...")

def toolchains(*, basename, toolchain_type, variable = None):
    toolchain_type = native.package_relative_label(toolchain_type)
    variable = variable or basename.upper()

    [
        (
            toolchain_symlink_target(
                name = "busybox-{}-{}-tool".format(cpu, os),
                target = "@busybox-{}-{}//:busybox".format(cpu, os),
                basename = basename,
                tags = ["manual"],
            ),
            toolchain_info(
                name = "busybox-{}-{}-info".format(cpu, os),
                target = ":busybox-{}-{}-tool".format(cpu, os),
                variable = variable,
                tags = ["manual"],
            ),
            native.toolchain(
                name = "busybox-{}-{}".format(cpu, os),
                exec_compatible_with = [
                    "@toolchain_utils//toolchain/constraint/cpu:{}".format(cpu),
                    "@toolchain_utils//toolchain/constraint/os:{}".format(os),
                ],
                toolchain = "busybox-{}-{}-info".format(cpu, os),
                toolchain_type = toolchain_type,
            ),
        )
        for cpu, os in (
            ("amd64", "linux"),
            ("arm64", "linux"),
        )
    ]
