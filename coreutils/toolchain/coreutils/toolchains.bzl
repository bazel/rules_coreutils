load("@toolchain_utils//toolchain/symlink/target:defs.bzl", "toolchain_symlink_target")
load("@toolchain_utils//toolchain/info:defs.bzl", "toolchain_info")

visibility("//coreutils/...")

def toolchains(*, basename, toolchain_type, variable = None):
    toolchain_type = native.package_relative_label(toolchain_type)
    variable = variable or basename.upper()

    [
        (
            toolchain_symlink_target(
                name = "coreutils-{}-{}-{}-tool".format(cpu, os, libc),
                target = "@coreutils-{}-{}-{}//:entrypoint".format(cpu, os, libc),
                basename = basename,
                tags = ["manual"],
            ),
            toolchain_info(
                name = "coreutils-{}-{}-{}-info".format(cpu, os, libc),
                target = ":coreutils-{}-{}-{}-tool".format(cpu, os, libc),
                variable = variable,
                tags = ["manual"],
            ),
            native.toolchain(
                name = "coreutils-{}-{}-{}".format(cpu, os, libc),
                exec_compatible_with = [
                    "@toolchain_utils//toolchain/constraint/cpu:{}".format(cpu),
                    "@toolchain_utils//toolchain/constraint/os:{}".format(os),
                ],
                toolchain = "coreutils-{}-{}-{}-info".format(cpu, os, libc),
                toolchain_type = toolchain_type,
            ),
        )
        for cpu, os, libc in (
            ("amd64", "linux", "musl"),
            ("arm64", "linux", "musl"),
            ("amd64", "windows", "msvc"),
            ("amd64", "macos", "darwin"),
            ("arm64", "macos", "darwin"),
        )
    ]
