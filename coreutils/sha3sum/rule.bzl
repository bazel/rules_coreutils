visibility("//coreutils/...")

ATTRS = {
    "srcs": attr.label_list(
        doc = "Create SHA3 sum for each file.",
        allow_files = True,
        allow_empty = False,
        mandatory = True,
    ),
    "_template": attr.label(
        default = "//coreutils/redirect:stdout",
        allow_single_file = True,
    ),
}

def implementation(ctx):
    toolchain = ctx.toolchains["//coreutils/toolchain/sha3sum:type"]

    output = ctx.actions.declare_file(ctx.label.name)
    rendered = ctx.actions.declare_file("{}.{}".format(ctx.label.name, ctx.file._template.extension))

    ctx.actions.expand_template(
        template = ctx.file._template,
        output = rendered,
        is_executable = True,
        substitutions = {
            "{{stdout}}": output.path,
        },
    )

    args = ctx.actions.args()
    args.add(toolchain.executable.path)
    args.add_all(ctx.files.srcs)

    ctx.actions.run(
        inputs = ctx.files.srcs,
        tools = [toolchain.run],
        executable = rendered,
        arguments = [args],
        outputs = [output],
        mnemonic = "Sha1Sum",
        progress_message = "sha3sum",
        toolchain = "//coreutils/toolchain/sha3sum:type",
    )

    default = DefaultInfo(
        files = depset([output]),
        runfiles = ctx.runfiles([output]),
    )

    return [default]

coreutils_sha3sum = rule(
    doc = "Calculates SHA3 sums for a collection of files.",
    attrs = ATTRS,
    implementation = implementation,
    toolchains = ["//coreutils/toolchain/sha3sum:type"],
)

sha3sum = coreutils_sha3sum
